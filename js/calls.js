export default calls = [
  {
    date: 'Wed. Jan 11. 2017',
    calls: [
      {
        hour: '6:37 pm',
        name: 'Don Ernesto Martínez',
        place: 'Home',
        type: 'mis'
      },
      {
        hour: '9:00 pm',
        name: 'Juan Rodrigo Pérez',
        place: 'Mobile',
        type: 'rem'
      },
      {
        hour: '11:02 pm',
        name: 'Adrián Ortega',
        place: 'iPhone',
        type: 'inc'
      },
      {
        hour: '11:34 pm',
        name: 'José Ortiz',
        place: 'Office',
        type: 'new'
      },
    ]
  },
  {
    date: 'Thu. Jan 12. 2017',
    calls: [
      {
        hour: '9:45 am',
        name: 'Aline Fonseca',
        place: 'iPhone',
        type: 'mis'
      },
      {
        hour: '12:01 pm',
        name: 'Jorge Peralta',
        place: 'Home',
        type: 'rem'
      },
      {
        hour: '07:56 pm',
        name: 'Eduardo López',
        place: 'iPhone',
        type: 'inc'
      },
      {
        hour: '10:11 pm',
        name: 'Adrián Ortega',
        place: 'Office',
        type: 'new'
      },
    ]
  },
  {
    date: 'Fri. Jan 13. 2017',
    calls: [
      {
        hour: '6:37 pm',
        name: 'Don Ernesto Martínez',
        place: 'Home',
        type: 'mis'
      },
      {
        hour: '9:00 pm',
        name: 'Juan Rodrigo Pérez',
        place: 'Mobile',
        type: 'rem'
      },
      {
        hour: '11:02 pm',
        name: 'Adrían Ortega',
        place: 'iPhone',
        type: 'inc'
      },
      {
        hour: '11:34 pm',
        name: 'José Ortiz',
        place: 'Office',
        type: 'new'
      },
    ]
  },
];
