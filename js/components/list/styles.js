const React = require('react-native');

const { StyleSheet, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;

module.exports = StyleSheet.create({
  container: {
    backgroundColor: '#F3F3F9',
    flex: 1,
    alignSelf: 'stretch'
  },
  contentCenter: {
    alignItems: 'center'
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 80
  },
  shadow: {
    flex: 1,
    width: null,
    height: null,
  },
  bg: {
    marginTop: 20
  },
  input: {
    marginBottom: 20,
  },
  contentRight: {
    left: 300
  },
  txtLink: {
    textAlign: 'center',
    color: '#C5E4ED',
    textDecorationLine: 'underline'
  },
  txtColumn: {
    alignSelf: 'center',
  },
  txtRow: {
    marginTop: 40,
    marginBottom: 50,
    flexDirection: 'row'
  },
  txtDont: {
    color: '#C5E4ED',
    marginRight: 4
  },
  txtWhite: {
    color: 'white'
  },
  btn: {
    shadowOpacity: 0,
    backgroundColor: '#61B6D2'
  },
  txtBtn: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold'
  },  
  liContent: {
    marginTop: 50,
    marginLeft: 30,
    marginBottom: 50
  },
  liTxt: {
    fontSize: 15,
    color: '#C5E4ED'
  },
  btnRow: {
    alignSelf: 'center',
    flexDirection: 'row'
  },
  btnBack: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
    marginRight: 210
  },
  btnNext: {
    color: '#C5E4ED',
    fontSize: 22,
    fontWeight: 'bold'
  },
  space: {
    marginBottom: 40
  },
  dividerStyle: {
    marginTop: 10,
    backgroundColor: '#F3F3F9',
    borderBottomWidth: 0,
    height: 10
  },
  dividerTxt: {
    color: '#276A80',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  eventStyle: {
    flexDirection: 'column',
    backgroundColor: '#E6EEF1',
    borderRadius: 25,
    alignSelf: 'stretch',
    borderBottomWidth: 0,
    paddingTop: 11,
    paddingBottom: 11,
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    position: 'relative'
  },
  txtEvent: {
    paddingLeft: 20,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#276A80',
    alignSelf: 'flex-start',
    backgroundColor: 'transparent'
  },
  iconStyle: {
    fontSize: 22,
    color: '#276A80',
    alignSelf: 'flex-end',
    marginRight: 20
  },
  borderList: {
    borderBottomWidth: 0,
    flexDirection: 'row',
    alignSelf: 'stretch'
  },
  panelClose: {
    alignItems: 'center',
    height: 60,
    flexDirection: 'row',
    backgroundColor: '#46A2B7',
    paddingTop: 10
  },
  txtClose: {
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  iconClose: {
    marginLeft: 15,
    marginRight: 65,
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  panelSearch: {
    height: 60,
    backgroundColor: '#60B6D1'
  },
  searchStyle: {
    marginLeft: 15,
    paddingLeft: 20,
    width: 345,
    height: 40,
    borderRadius: 18,
    backgroundColor: '#C5E4ED'
  },
  cardsEnd: {
    justifyContent: 'flex-end'
  },
  cardHour: {    
    backgroundColor: '#60B6D1',
    flexDirection: 'row'
  },
  cardRemind: {
    height: 100,
    backgroundColor: '#276A80'
  },
  scrollStyle: {
    height: 60,
    backgroundColor: '#F4F4F9',
  },
  scrollRow: {
    flexDirection: 'row'
  },
  bs: {
    marginLeft: 15,
    borderRadius: 25,
    height: 50,
    width: 60,
    backgroundColor: '#286A80',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  txtBs: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white'
  },
  numbr: {
    marginLeft: 10,
    borderRadius: 25,
    height: 50,
    width: 130,
    backgroundColor: '#C5E4ED',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  txtNumber: {
    color: '#6297A8'
  }
});