import React, { Component } from 'react';
import { Image, TextInput, Text, Linking, ScrollView, TouchableOpacity, TouchableHighlight, View as RView } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, InputGroup, Input, Button, View, List, ListItem } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import TextField from 'react-native-md-textinput';
import navigateTo from '../../actions/sideBarNav';
import { setUser } from '../../actions/user';
import styles from './styles';

const {
  replaceAt,
  pushRoute,
} = actions;

const contacts = [
  {
    order: 0,
    letter: 'A',
    elements: [
      "Ángel Ramos D.",
      "Alberto Bessoudo Sustiel",
      "Álvaro Sánchez Medina A.",
      "Bernardo Ramos D."
    ]
  },
  {
    order: 1,
    letter: 'B',
    elements: [
      "Bernardo Ramos D."
    ]
  }
];

class CustomList extends Component {

  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
    pushRoute: React.PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      index: -1,
      filter: ''
    };
  }

  setUser(name) {
    this.props.setUser(name);
  }

  replaceRoute(route) {
    this.props.replaceAt('login', { key: route }, this.props.navigation.key);
  }

  setIndex = (i) => {
    if(i === this.state.index) {
      this.setState({index: -1});
    } else {
      this.setState({index: i});
    }
  }

  next = () => {
    this.props.navigateTo('reminder', 'calendar');
  }

  render() {
    const elems = [];
    let i = 0;
    contacts.forEach((element, indexElement) => {
      elems.push(
        <ListItem itemDivider key={elems.length}  style={styles.dividerStyle}>
          <Text style={styles.dividerTxt}>- {element.letter} -</Text>
        </ListItem>
      );
      i++;
      element
        .elements
        .filter((contact) => (contact.toLowerCase().indexOf(this.state.filter.toLowerCase()) >= 0))
        .forEach((contact, indexContact) => {
        if(indexElement + indexContact === this.state.index){
          elems.push(
            <View style={styles.borderList} key={elems.length}>
              <View style={styles.scrollStyle}>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  <View style={styles.scrollRow}>
                    <TouchableHighlight
                      onPress={() => {
                        this.setState({index: -1});
                      }}
                      style={styles.bs}>
                      <Text style={styles.txtBs}>BS</Text>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={() => this.next()} style={styles.numbr}>
                      <RView style={{alignItems: 'center'}}>
                        <Text style={styles.txtNumber}>home</Text>
                        <Text style={styles.txtNumber}>5570 5480</Text>
                      </RView>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={() => this.next()} style={styles.numbr}>
                      <RView style={{alignItems: 'center'}}>
                        <Text style={styles.txtNumber}>iPhone</Text>
                        <Text style={styles.txtNumber}>(55) 1078 1823</Text>
                      </RView>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={() => this.next()} style={styles.numbr}>
                      <RView style={{alignItems: 'center'}}>
                        <Text style={styles.txtNumber}>iPhone</Text>
                        <Text style={styles.txtNumber}>(55) 1078 1823</Text>
                      </RView>
                    </TouchableHighlight>
                  </View>
                </ScrollView>
              </View>
            </View>
          );
        } else {
          elems.push(
            <ListItem key={elems.length} style={styles.borderList}>
              <TouchableHighlight
                onPress={() => {
                  this.setIndex(indexElement + indexContact);
                }}
                underlayColor='#46A2B7'
                style={styles.eventStyle}>
                <RView
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'stretch',
                    justifyContent: 'space-between',
                  }}
                  >
                  <Text style={styles.txtEvent}>{contact}</Text>
                  <Icon name="angle-down" style={styles.iconStyle}/>
                </RView>
              </TouchableHighlight>
            </ListItem>
          );
        }
        i++;
      });
    });
    return (
      <Container>
        <View style={styles.container}>
          <Content>
            <View style={styles.panelClose}>
              <TouchableHighlight
                onPress={() => this.props.navigateTo('calendar', 'calendar')}
                >
                <RView>
                  <Icon name="times-circle" style={styles.iconClose} />
                </RView>
              </TouchableHighlight>
              <Text style={styles.txtClose}>Who do you want to call?</Text>
            </View>
            <View style={[styles.panelSearch]}>
              <TextInput
                placeholder="Search"
                placeholderTextColor="#61B6D2"
                style={[styles.searchStyle, {marginTop: 10, marginBottom: 10}]}
                onChangeText={(filter) => this.setState({filter: filter})}
                />
            </View>
            <List>
              {elems}
            </List>
          </Content>
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: name => dispatch(setUser(name)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(CustomList);
