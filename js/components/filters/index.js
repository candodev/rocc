import React, { Component } from 'react';
import { Image, Text, Linking, ScrollView, TouchableHighlight, View as NView } from 'react-native';
import { Container, Content, InputGroup, Input, Button,  View, Header, Title } from 'native-base';
import styles from './styles';

class CustomCalendar extends Component {
  render() {
    let allStyle = {};
    let newStyle = {};
    let remStyle = {};
    let incStyle = {};
    let misStyle = {};

    let allTxt = {};
    let newTxt = {};
    let remTxt = {};
    let incTxt = {};
    let misTxt = {};
    switch (this.props.selected) {
      case 'all':
        allStyle = styles.callsSelect;
        allTxt = styles.txtCallsSelect;
        break;
      case 'new':
        newStyle = styles.callsSelect;
        newTxt = styles.txtCallsSelect;
        break;
      case 'rem':
        remStyle = styles.callsSelect;
        remTxt = styles.txtCallsSelect;
        break;
      case 'inc':
        incStyle = styles.callsSelect;
        incTxt = styles.txtCallsSelect;
        break;
      case 'mis':
        misStyle = styles.callsSelect;
        misTxt = styles.txtCallsSelect;
        break;
    }
    return (
      <View style={styles.scrollCall}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          <View style={styles.scrollRow}>
            <TouchableHighlight onPress={() => this.props.setFilter('all')} style={[styles.calls, allStyle]}>
              <Text style={[styles.txtCalls, allTxt]}>All</Text>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.props.setFilter('new')} style={[styles.calls, newStyle]}>
              <NView>
                <Text style={[styles.txtCalls, newTxt]}>New{'\n'}Contacts</Text>
                <View style={[styles.line, styles.yellowBG]}></View>
              </NView>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.props.setFilter('rem')} style={[styles.calls, remStyle]}>
              <NView>
              <Text style={[styles.txtCalls, remTxt]}>Call{'\n'}reminders</Text>
              <View style={[styles.line, styles.purpleBG]}></View>
              </NView>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.props.setFilter('inc')} style={[styles.calls, incStyle]}>
              <NView>
              <Text style={[styles.txtCalls, incTxt]}>Incoming{'\n'}calls</Text>
              <View style={[styles.line, styles.greenBG]}></View>
              </NView>
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.props.setFilter('mis')} style={[styles.calls, misStyle]}>
              <NView>
              <Text style={[styles.txtCalls, misTxt]}>Missed{'\n'}calls</Text>
              <View style={[styles.line, styles.redBG]}></View>
              </NView>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    );    
  }
}

export default CustomCalendar;