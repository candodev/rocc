
const React = require('react-native');

const { StyleSheet, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;

module.exports = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F9',
    flex: 1,
    position: 'relative'
  },
  eventStyle: {
    marginTop: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: '#E7EFF1',
    borderRadius: 25,
    height: 80,
    alignSelf: 'stretch'
  },
  hrsStyle: {
    paddingLeft: 10,
    flex: 0.2,
    fontWeight: 'bold',
    color: '#286A80'
  },
  separator: {
    marginRight: 10,
    width: 2,
    height: 43
  },
  miniCircle: {
    height: 7,
    width: 7,
    borderRadius: 50,
    marginRight: 20
  },
  iconStyle: {
    fontSize: 17,
    color: '#286A80',
    flex: 0.1,
  },
  infoColumn: {
    flexDirection: 'column',
    flex: 0.6
  },
  eventTitle: {
    fontSize: 16,
    fontWeight: '500',
    color: '#286A80'
  },
  eventInfo: {
    fontWeight: '500',
    color: '#286A80'
  },
  target: {
    position: 'absolute',
    backgroundColor: '#62DCFA',
    justifyContent: 'flex-end'
  },
  firstPadd: {
    paddingTop: 30,
    paddingLeft: 30
  },
  secPadd: {
    paddingLeft: 5
  },
  targetT: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#286A80'
  },
  targetP: {
    marginTop: 15,
    marginBottom: 20,
    fontWeight: 'bold',
    color: '#3790AC'
  },
  btnGo: {
    justifyContent: 'center',
    width: 85,
    height: 45,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: '#62DCFA',
    borderColor: 'white',
    marginBottom: 20
  },
  txtGo: {
    fontSize: 16,
    textAlign: 'center',
    color: 'white'
  },
  circAbs: {
    top: 125,
    left: 220,
    position: 'absolute'
  },
  bigCircle: {
    position: 'relative',
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: '#6AE4F9'
  },
  smallCircle: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 28,
    left: 28,
    height: 45,
    width: 45,
    borderRadius: 50,
    backgroundColor: '#47A3B8'
  },
  iconAlarm: {
    fontSize: 30,
    color: 'white'
  },
  scrollStyle: {
    height: 60,
    backgroundColor: '#61B6D2',
  },
  scrollRow: {
    flexDirection: 'row'
  },
  month: {
    marginLeft: 5,
    borderRadius: 24,
    height: 50,
    width: 80,
    backgroundColor: '#C5E4ED',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  txtMonth: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#4195B0'
  },
  monthAct: {
    marginLeft: 5,
    borderRadius: 24,
    height: 50,
    width: 80,
    backgroundColor: '#47A3B8',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  txtMonthAct: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white'
  },

  scrollCall: {
    height: 90,
    backgroundColor: '#61B6D2',
  },
  calls: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    borderRadius: 20,
    height: 80,
    width: 80,
    backgroundColor: '#C5E4ED',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  txtCalls: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#4195B0'
  },
  callsSelect: {
    marginLeft: 5,
    borderRadius: 24,
    height: 80,
    width: 80,
    backgroundColor: '#47A3B8',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  txtCallsSelect: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white'
  },
  line: {
    marginTop: 7,
    height: 1,
    width: 50,
    alignSelf: 'center',
  },
  yellowBG: {
    backgroundColor: '#EAF756'
  },
  purpleBG: {
    backgroundColor: '#9174F9'
  },
  greenBG: {
    backgroundColor: '#59FF34'
  },
  redBG: {
    backgroundColor: '#ED5456'
  },
  today: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#61B6D2',
    height: 50
  },
  txtToday: {
    color: 'white',
    fontSize: 20
  },
  lineToday: {
    marginLeft: 71,
    marginRight: 71,
    width: 1,
    height: 53,
    backgroundColor: 'white'
  },
  todayCircle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: 45,
    borderRadius: 50,
    backgroundColor: '#47A3B8'
  },
  infoDay: {
    marginLeft: 30,
    fontWeight: 'bold',
    color: '#286A80',
    textDecorationLine: 'underline'
  }
});
