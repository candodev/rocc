import React, {Component} from 'react';
import { Image, Text, Linking, ScrollView, TouchableHighlight, View as NView } from 'react-native';
import { Container, Content, InputGroup, Input, Button,  View, Header, Title } from 'native-base';
import styles from '../calendar/styles';
import Icons from 'react-native-vector-icons/MaterialIcons';
import calls from '../../calls';

class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: ''
    };
  }

  toggleSelected = (item) => {
    if(item === this.state.selected) {
      this.setState({selected: ''});
    } else {
      this.setState({selected: item});
    }
  }
  render() {
    return (
      <ScrollView>
        {calls.map((current, indexLevel) => {
          return (
            <View key={indexLevel}>
              <Text style={styles.infoDay}>{current.date}</Text>
              {current.calls.map((call, index) => {
                if(this.props.currentFilter === 'all' || this.props.currentFilter === call.type){
                  let objStyle = {};
                  let className = {};
                  switch(call.type){
                    case 'mis':
                      objStyle = styles.redBG;
                      className = 'call-missed';
                      break;
                    case 'rem':
                      objStyle = styles.purpleBG;
                      className = 'alarm';
                      break;
                    case 'inc':
                      objStyle = styles.greenBG;
                      className = 'call-received';
                      break;
                    case 'new':
                      objStyle = styles.yellowBG;
                      className = 'contacts';
                      break;
                  }
                  return (
                    <TouchableHighlight
                      key={index}
                      onPress={() => this.toggleSelected('' + indexLevel + index)}
                      >
                      <NView style={styles.eventStyle}>
                        <Text style={styles.hrsStyle}>{call.hour}</Text>
                        <View style={[styles.separator, objStyle]}></View>
                        <View style={styles.infoColumn}>
                          <Text style={styles.eventTitle}>{call.name}</Text>
                          <Text style={styles.eventInfo}>{call.place}</Text>
                        </View>
                        <View style={[styles.miniCircle, objStyle]}></View>
                        <Icons name={className} style={styles.iconStyle} />
                      </NView>
                    </TouchableHighlight>
                  );
                }
              })}
            </View>
          );
        })}
      </ScrollView>
    );
  }
}

export default Events;