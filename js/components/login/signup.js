import React, { Component } from 'react';
import { Image, Text, Linking } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, InputGroup, Input, Button, Icon, View } from 'native-base';
import TextField from 'react-native-md-textinput';
import axios from 'axios';
import { setUser } from '../../actions/user';
import {API_BASE} from '../../constants/api.js';
import styles from './styles';

const {
  replaceAt,
  popRoute
} = actions;

class Signup extends Component {

  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      name: '',
      email: '',
      password: '',
      passwordRepeat: '',
      showErrorFields: false,
      showErrorServer: false,
      serverError: '',
      loading: true
    };
  }

  setUser(name) {
    this.props.setUser(name);
  }

  replaceRoute(route) {
    this.setUser(this.state.name);
    this.props.replaceAt('signup', { key: route }, this.props.navigation.key);
  }

  popRoute() {
    this.props.popRoute(this.props.navigation.key);
  }

  next = () => {
    if(this.state.index + 1 <= 3) {
      this.setState({index: this.state.index + 1});
    }
  }

  prev = () => {
    if(this.state.index - 1 >= 0) {
      this.setState({index: this.state.index - 1});
    }
  }

  fieldStyle = (field) => {
    if(field === '') {
      return {
        opacity: 0.5
      };
    } else {
      return {
        opacity: 1
      };
    }
  }

  submit = () => {
    if(this.state.password !== this.state.passwordRepeat) {
      this.setState({showErrorFields: true});
      return;
    } else {
      const requestObj = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
      };
      fetch(`${API_BASE}api/RoccUsers`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestObj)
      })
      .then((responseJson) => {
        this.replaceRoute('calendar');
      })
      .catch((error) => {
        console.log('bad');
        console.log(error);
      })
    }
  }

  render() {

    const next1Style = this.fieldStyle(this.state.name);
    const next2Style = this.fieldStyle(this.state.email);

    const steps = [
      (
        <View>
          <View style={styles.contentCenter}>
            <Text style={styles.counter}>1/3</Text>
            <Text style={styles.title}>Whats your name?</Text>
            <View style={styles.bg}>
              <TextField
                style={styles.tfSize}
                onChangeText={(name) => {
                  this.setState({name: name})
                }}
                value={this.state.name}
                label={'John Smith...'}
                labelStyle={{fontSize: 20}}
                highlightColor={'#C5E4ED'}
                textBlurColor={'white'}
                textFocusColor={'#C5E4ED'}
                labelColor={'#C5E4ED'}
                borderColor={'white'}
                width={230}
                height={40}
                dense={true}
                autoFocus={true}
              />
            </View>
          </View>
          <View style={styles.txtColumn}>
            <View style={styles.txtRow}>
              <Text style={styles.txtDont}>Already have an account?</Text>
              <Text style={styles.txtLink} onPress={() => this.replaceRoute('login')}>Sign in.</Text>
            </View>
          </View>
        </View>
      ),(
        <View>
          <View style={styles.steps}>
            <View style={[styles.step, styles.stepFilled]}></View>
            <View style={styles.step}></View>
            <View style={styles.step}></View>
          </View>
          <View style={styles.contentCenter}>
            <Text style={styles.counter}>2/3</Text>
            <Text style={styles.title}>And, your email address?</Text>
            <View style={styles.bg}>
              <TextField
                style={styles.tfSize}
                label={'user@example.com'}
                labelStyle={{fontSize: 20}}
                highlightColor={'#C5E4ED'}
                textBlurColor={'white'}
                textFocusColor={'#C5E4ED'}
                labelColor={'#C5E4ED'}
                borderColor={'white'}
                width={230}
                height={40}
                dense={true}
                value={this.state.email}
                onChangeText={(email) => {
                  this.setState({email: email});
                }}
                keyboardType={'email-address'}
                autoFocus={true}
              />
            </View>
          </View>
          <View style={styles.space}></View>
          <View style={styles.txtColumn}>
            <View style={styles.btnRow}>
              <Text style={styles.txtDont}>By tapping "Next" you agree to the</Text>
              <Text style={styles.txtWhite}>Terms and</Text>
            </View>
            <View style={styles.btnRow}>
              <Text style={styles.txtWhite}>Conditions </Text>
              <Text style={styles.txtDont}>and the </Text>
              <Text style={styles.txtWhite}>Privacy policy.</Text>
            </View>
          </View>
          <View style={styles.space}></View>
        </View>
      ),(
        <View>
          <View style={styles.steps}>
            <View style={[styles.step, styles.stepFilled]}></View>
            <View style={[styles.step, styles.stepFilled]}></View>
            <View style={styles.step}></View>
          </View>
          <View style={styles.contentCenter}>
            <Text style={styles.counter}>3/3</Text>
            <Text style={styles.title}>Now, create your password!</Text>
            <View style={styles.bg}>
              <TextField
                style={styles.tfSize}
                secureTextEntry={true}
                label={'Password...'}
                labelStyle={{fontSize: 20}}
                highlightColor={'#C5E4ED'}
                textBlurColor={'white'}
                textFocusColor={'#C5E4ED'}
                autoFocus={true}
                height={40}
                labelColor={'#C5E4ED'}
                borderColor={'white'}
                value={this.state.password}
                onChangeText={(password) => {
                  this.setState({password: password});
                }}
              />
              <TextField
                style={styles.tfSize}
                secureTextEntry={true}
                label={'Re-enter password...'}
                labelStyle={{fontSize: 20}}
                highlightColor={'#C5E4ED'}
                textBlurColor={'white'}
                textFocusColor={'#C5E4ED'}
                labelColor={'#C5E4ED'}
                borderColor={'white'}
                width={230}
                height={40}
                value={this.state.passwordRepeat}
                onChangeText={(passwordRepeat) => {
                  this.setState({passwordRepeat: passwordRepeat});
                }}
              />
              {this.state.showErrorFields ? <Text>Las contraseñas no coinciden</Text> : null}
              {this.state.showErrorServer ? <Text>Algun error</Text> : null}
            </View>
          </View>
          <View style={styles.liContent}>
            <Text style={styles.liTxt}>• Be at least 6 characters.</Text>
            <Text style={styles.liTxt}>• Must be alphanumeric.</Text>
          </View>
        </View>
      )
    ];

    const btn = [
      (
        <View style={styles.contentRight}>
          <Button style={styles.btn} onPress={() => this.next()}>
            <Text style={[styles.btnNext, next1Style]}>NEXT</Text>
          </Button>
        </View>
      ),(
        <View style={styles.btnRow}>
          <Button style={styles.btn} onPress={() => this.prev()}>
            <Text style={styles.btnBack}>BACK</Text>
          </Button>
          <Button style={styles.btn} onPress={() => this.next()}>
            <Text style={[styles.btnNext, next2Style]}>NEXT</Text>
          </Button>
        </View>
      ),(
        <View style={styles.btnRow}>
          <Button style={styles.btn} onPress={() => this.prev()}>
            <Text style={styles.btnBack}>BACK</Text>
          </Button>
          <Button style={styles.btn} onPress={() => this.submit()}>
            <Text style={styles.btnNext}>DONE</Text>
          </Button>
        </View>
      )
    ]

    return (
      <Container>
        <View style={styles.container}>
          <Content scrollEnabled={false}>
            {steps[this.state.index]}
            {btn[this.state.index]}
          </Content>
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: name => dispatch(setUser(name)),
    popRoute: key => dispatch(popRoute(key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation
});

export default connect(mapStateToProps, bindActions)(Signup);
