import React, { Component } from 'react';
import { Image, Text, Linking, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, InputGroup, Input, Button, Icon, View } from 'native-base';
import TextField from 'react-native-md-textinput';
import axios from 'axios';
import { setUser } from '../../actions/user';
import styles from './styles';
import {API_BASE} from '../../constants/api.js';

const {
  replaceAt,
  popRoute
} = actions;

class Login extends Component {

  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      index: 0,
      email: '',
      password: '',
      showErrorEmptyFields: false,
      showErrorFailedLogin: false
    };
  }

  setUser(name) {
    this.props.setUser(name);
  }

  replaceRoute(route) {
    this.setUser(this.state.name);
    this.props.replaceAt('login', { key: route }, this.props.navigation.key);
  }

  popRoute() {
    this.props.popRoute(this.props.navigation.key);
  }

  next = () => {
    if(this.state.index + 1 <= 3) {
      this.setState({index: this.state.index + 1});
    }
  }

  prev = () => {
    if(this.state.index - 1 >= 0) {
      this.setState({index: this.state.index - 1});
    }
  }

  submit = () => {
    if(this.state.email === '' || this.state.password === '') {
      this.setState({showErrorEmptyFields: true});
      return;
    } else {
      const requestObj = {
        email: this.state.email,
        password: this.state.password,
      };
      axios.post(`${API_BASE}api/RoccUsers/login`,requestObj)
      .then((response) => {
        this.replaceRoute('calendar');
      })
      .catch((error) => {
        console.log('bad');
        console.log(error);
        this.setState({showErrorFailedLogin: true});
      })
    }
  }

  render() {
    return (
      <Container>
        <View style={styles.container}>
          <Content scrollEnabled={false}>
            <View>
              <View style={styles.contentCenter}>
                <Text style={styles.title}>Sign in to your ROCC account.</Text>
                <View style={styles.bg}>
                  <TextField
                    style={styles.tfSize}
                    onChangeText={email => this.setState({ email })}
                    value={this.state.email}
                    label={'Email Address...'}
                    labelStyle={{fontSize: 20}}
                    highlightColor={'#C5E4ED'}
                    textBlurColor={'white'}
                    textFocusColor={'#C5E4ED'}
                    labelColor={'#C5E4ED'}
                    borderColor={'white'}
                    width={230}
                    height={40}
                    autoFocus={true}
                    dense={true}
                    keyboardType={'email-address'}
                  />
                  <TextField
                    style={styles.tfSize}
                    height={40}
                    secureTextEntry={true}
                    onChangeText={password => this.setState({ password })}
                    value={this.state.password}
                    label={'Password...'}
                    labelStyle={{fontSize: 20}}
                    highlightColor={'#C5E4ED'}
                    textBlurColor={'white'}
                    textFocusColor={'#C5E4ED'}
                    labelColor={'#C5E4ED'}
                    borderColor={'white'}
                    dense={true}
                  />
                </View>
                <View>
                  {this.state.showErrorEmptyFields ? <Text>Todos los campos son obligatorios</Text> : null}
                  {this.state.showErrorFailedLogin ? <Text>El nombre de usuario o contraseña son inválidos</Text> : null}
                </View>
              </View>
              <View style={styles.txtColumn}>
                <Text style={styles.txtLink} onPress={() => Linking.openURL('http://google.com')}>Reset password</Text>
                <View style={styles.txtRow}>
                  <Text style={styles.txtDont}>Don't have an account?</Text>
                  <Text style={styles.txtLink} onPress={() => this.replaceRoute('signup')}>Sign up.</Text>
                </View>
              </View>
            </View>
            <View style={styles.contentRight}>
              <Button style={styles.btn} onPress={() => this.submit()}>
                <Text style={styles.txtBtn}>NEXT</Text>
              </Button>
            </View>
          </Content>
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: name => dispatch(setUser(name)),
    popRoute: key => dispatch(popRoute(key)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(Login);
