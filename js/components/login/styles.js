const React = require('react-native');

const { StyleSheet, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;

module.exports = StyleSheet.create({
  container: {
    backgroundColor: '#61B6D2',
    flex: 1,
  },
  contentCenter: {
    alignItems: 'center'
  },
  counter: {
    marginTop: 40,
    color: 'white'
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 20
  },
  shadow: {
    flex: 1,
    width: null,
    height: null,
  },
  bg: {
    marginTop: 20
  },
  input: {
    marginBottom: 20,
  },
  contentRight: {
    left: 300
  },
  txtLink: {
    textAlign: 'center',
    color: '#C5E4ED',
    textDecorationLine: 'underline'
  },
  txtColumn: {
    marginTop: 20,
    alignSelf: 'center',
  },
  txtRow: {
    marginTop: 30,
    marginBottom: 50,
    flexDirection: 'row'
  },
  txtDont: {
    color: '#C5E4ED',
    marginRight: 4
  },
  txtWhite: {
    color: 'white'
  },
  btn: {
    backgroundColor: '#61B6D2',
    elevation: 0,
    shadowOpacity: 0
  },
  txtBtn: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold'
  },  
  liContent: {
    marginTop: 40,
    marginLeft: 30,
    marginBottom: 40
  },
  liTxt: {
    fontSize: 15,
    color: '#C5E4ED'
  },
  btnRow: {
    alignSelf: 'center',
    flexDirection: 'row'
  },
  btnBack: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
    marginRight: 210
  },
  btnNext: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold'
  },
  space: {
    marginBottom: 40
  },
  steps: {
    flexDirection: 'row',
    height: 20
  },
  step: {
    flex: 1
  },
  stepFilled: {
    backgroundColor: '#3790AC',
    height: 40
  },
  tfSize: {
    fontSize: 20
  }
});