import React, { Component } from 'react';
import { Image, Text, Linking, ScrollView, TouchableHighlight, View as NView } from 'react-native';
import { Container, Content, InputGroup, Input, Button,  View, Header, Title } from 'native-base';

class CustomTitle extends Component {
  render() {
    let text = '';
    switch (this.props.selected) {
      case 'all':
        text = 'All';
        break;
      case 'new':
        text = 'New Contacts';
        break;
      case 'rem':
        text = 'Call Reminders';
        break;
      case 'inc':
        text = 'Incoming Calls';
        break;
      case 'mis':
        text = 'Missed Calls';
        break;
      
    }
    return <Text style={{color: 'white', fontSize: 18}}>{text}</Text>;
  }
}

export default CustomTitle;