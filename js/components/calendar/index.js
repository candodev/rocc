
import React, { Component } from 'react';
import { Image, Text, Linking, ScrollView, TouchableHighlight, View as NView } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, InputGroup, Input, Button,  View, Header, Title } from 'native-base';
import TextField from 'react-native-md-textinput';
import Calendar from 'react-native-calendar';
import Icon from 'react-native-vector-icons/FontAwesome';
import { setUser } from '../../actions/user';
import { openDrawer } from '../../actions/drawer';
import styles from './styles';
import Icons from 'react-native-vector-icons/MaterialIcons';
import Filters from '../filters';
import CustomTitle from './customtitle';
import Events from '../events';
import { setIndex } from '../../actions/list';

const calendarDays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
const calendarMonths = ['Jan', 'Feb', 'Mar', 'Apr' , 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const {
  replaceAt,
  popRoute,
  pushRoute
} = actions;

class CustomCalendar extends Component {

  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    setIndex: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      showAdvice: true,
      showCalendar: true,
      showFilters: false,
      currentFilter: 'all'
    };
  }

  setUser(name) {
    this.props.setUser(name);
  }

  replaceRoute(route) {
    this.setUser(this.state.name);
    this.props.replaceAt('login', { key: route }, this.props.navigation.key);
  }

  popRoute() {
    this.props.popRoute(this.props.navigation.key);
  }

  pushRoute(route, index) {
    this.props.setIndex(index);
    this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
  }

  goToReminder = () => {
    this.pushRoute('list', 1);
  }

  hideAdvice = () => {
    this.setState({showAdvice: false});
  }

  card(){
    return (
        <View style={[styles.target, {bottom: 0, left: 0, right: 0}]}>
          <View style={styles.firstPadd}>
            <Text style={styles.targetT}>Add your call reminders!</Text>
            <View style={styles.secPadd}>
              <Text style={styles.targetP}>Lorem ipsum dolor sit amet,{'\n'}consectetur adipiscing elit, sed do{'\n'}eiusmod tempor incididunt.</Text>
              <Button style={styles.btnGo} onPress={() => this.hideAdvice()}>
                <Text style={styles.txtGo}>Go it</Text>
              </Button>
            </View>
            <View style={styles.circAbs}>
              <View style={styles.bigCircle}>
                <View style={styles.smallCircle}>
                  <Icons name="alarm-add" style={styles.iconAlarm} />
                </View>
              </View>
            </View>
          </View>
        </View>
      )
  }

  toggleCalendar = () => {
    this.setState({showCalendar: !this.state.showCalendar});
  }

  toggleFilters = () => {
    this.setState({showFilters: !this.state.showFilters});
  }

  setFilter = (filter) => {
    this.setState({currentFilter: filter});
  }

  render() {

    const customStyle = {
      calendarControls: {
        backgroundColor: '#61B6D2'
      },
      calendarHeading: {
        borderBottomColor: '#E6E6E6',
        borderTopColor: '#E6E6E6'
      },
      title: {
        color: '#61B6D2'
      },
      currentDayCircle: {
        backgroundColor: '#47A3B8'
      },
      selectedDayCircle: {
        backgroundColor: '#C5E4ED'
      },
      day: {
        color: '#286A80'
      },
      dayHeading: {
        fontWeight: 'bold',
        color: '#47A3B8'
      },
      weekendHeading: {
        fontWeight: 'bold',
        color: '#47A3B8'
      },
      eventIndicator: {
        backgroundColor: '#47A3B8',
        width: 5,
        height: 5,
        borderRadius: 50
      },
      header: {
        paddingTop: 10,
        backgroundColor: '#47A3B8',
        flexDirection: 'row'
      },
      icons: {
        color: '#fff'
      }
    }

    return (
      <Container>
        <View style={styles.container}>
          <View style={customStyle.header}>
            <Button style={{flex: 0.1}} transparent onPress={() => this.props.openDrawer()}>
              <Icon name="bars" style={customStyle.icons} size={20} />
            </Button>
            <View
              style={{
                flex: 0.8,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
              }}
              >
              <CustomTitle selected={this.state.currentFilter} />
              <Button
                style={{ backgroundColor: 'rgba(0,0,0,0)', elevation: 0}}
                onPress={() => this.toggleFilters() }
                >
                <Icon style={{color: 'white', marginLeft: 5}} name="caret-down" size={15}></Icon>
              </Button>
            </View>
            <Button style={{flex: 0.1}} transparent onPress={() => this.popRoute()}>
              <Icon name="search" style={customStyle.icons} size={20} />
            </Button>
          </View>
          {this.state.showFilters ? <Filters setFilter={this.setFilter} selected={this.state.currentFilter} /> : null}
          <View>
            <View style={styles.scrollStyle}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <View style={styles.scrollRow}>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Oct</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Nov</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Dec</Text>
                  </View>
                  <View style={styles.monthAct}>
                    <Text style={styles.txtMonthAct}>Jan</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Feb</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Mar</Text>
                  </View>
                </View>
              </ScrollView>
            </View>
            <View style={styles.scrollDay}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <View style={styles.scrollRow}>
                  <View style={styles.dayBasis}>
                    <Text style={styles.txtDay}>S</Text>
                  </View>
                  <View style={styles.dayBasis}>
                    <Text style={styles.txtDay}>M</Text>
                  </View>
                  <View style={styles.dayBasis}>
                    <Text style={styles.txtDay}>T</Text>
                  </View>
                  <View style={styles.dayBasis}>
                    <Text style={styles.txtDay}>W</Text>
                  </View>
                  <View style={styles.dayBasis}>
                    <Text style={styles.txtDay}>T</Text>
                  </View>
                  <View style={styles.dayBasis}>
                    <Text style={styles.txtDay}>F</Text>
                  </View>
                  <View style={styles.dayBasis}>
                    <Text style={styles.txtDay}>S</Text>
                  </View>
                </View>
              </ScrollView>
            </View>
            <View style={styles.scrollWeek}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <View style={styles.scrollRow}>
                  <View style={styles.weekBasis}>
                    <Text style={styles.txtLeftRight}>1</Text>
                  </View>
                  <View style={styles.weekBasis}>
                    <Text style={styles.txtWeek}>2</Text>
                  </View>
                  <View style={styles.weekBasis}>
                    <Text style={styles.txtWeek}>3</Text>
                  </View>
                  <View style={styles.weekBasis}>
                    <View style={styles.weekSelected}>
                      <Text style={styles.txtWeek}>4</Text>
                    </View>
                  </View>
                  <View style={styles.weekBasis}>
                    <Text style={styles.txtWeek}>5</Text>
                  </View>
                  <View style={styles.weekBasis}>
                    <Text style={styles.txtWeek}>6</Text>
                  </View>
                  <View style={styles.weekBasis}>
                    <Text style={styles.txtLeftRight}>7</Text>
                  </View>
                </View>
              </ScrollView>
            </View>
            <View>
              {this.state.showCalendar ? (
                <Calendar
                  showEventIndicators
                  customStyle={customStyle}
                  ref="calendar"
                  eventDates={['2017-01-03', '2017-01-06', '2017-01-10', '2017-01-15', '2017-01-21', '2017-01-25', '2017-01-28']}
                  events={[{date: '2016-07-04', hasEventCircle: {backgroundColor: 'powderblue'}}]}
                  scrollEnabled={true}
                  showControls={false}
                  dayHeadings={calendarDays}
                  monthNames={calendarMonths}
                  titleFormat={'YYYY'}
                  onDateSelect={(date) => this.setState({ selectedDate: date })}
                  weekStart={0}
                  style={{borderBottomColor: '#ccc', borderBottomWidth: 2}}
                />
              ) : null }
              <View style={{
                position: 'relative',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
              }}>
                <Button
                  style={{
                    marginTop: -12,
                    backgroundColor: '#3790ac',
                    width: 40,
                    height: 23,
                    borderRadius: 9
                  }}
                  onPress={() => this.toggleCalendar()}
                  >
                  <Text style={{color: 'white'}}>
                    <Icon name={this.state.showCalendar ? 'angle-up' : 'angle-down'} style={{fontSize: 20}}/>
                  </Text>
                </Button>
              </View>
              <Events currentFilter={this.state.currentFilter} />
            </View>
          </View>
          <View style={styles.today}>
            <Text style={styles.txtToday}>Today</Text>
            <View style={styles.lineToday}></View>
            <TouchableHighlight
              onPress={() => {
                
                this.goToReminder()
              }}
              style={styles.todayCircle}>
              <NView>
                <Icons name="alarm-add" style={styles.iconAlarm} />
              </NView>
            </TouchableHighlight>
          </View>
          {this.state.showAdvice ? this.card() : null}
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: name => dispatch(setUser(name)),
    openDrawer: () => dispatch(openDrawer()),
    popRoute: key => dispatch(popRoute(key)),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
    setIndex: index => dispatch(setIndex(index)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(CustomCalendar);
