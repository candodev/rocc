import React, {Component} from 'react';
import { Text, TouchableHighlight, View } from 'react-native';

export default class WatchNumber extends Component {
  render() {
    let back = null;
    let text = null;
    if(this.props.selected) {
      back = {backgroundColor: '#47A3B8'};
      text = {color: '#fff'};
    } else {
      text = {color: '#276a80'}
    }
    return (
      <TouchableHighlight style={[{
        position: 'absolute',
        }, this.props.style]}
        onPress={() => this.setStyle()}
        >
        <View
          style={[{
            width: 50,
            height: 50,
            borderRadius: 30,
            justifyContent: 'center'
            },
            back
          ]}
          >
          <Text style={[{
              fontSize: 18,
              fontWeight: 'bold',
              textAlign: 'center',
              backgroundColor: 'transparent'
            },
            text
          ]}>{this.props.number}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}