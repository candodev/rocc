import React, { Component } from 'react';
import { Image, Text, Linking, ScrollView, TouchableHighlight, AlertIOS, View } from 'react-native';
import WatchNumber from './WatchNumber.js';

const hours = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
const minutes = ['05', 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, '00'];
const numsStyle = [
  {right: 55, top: 33, marginTop: -10},
  {top: 75, right: 15, marginTop: -10},
  {right: 5, top: 127, marginTop: -10},
  {right: 60, bottom: 20, marginTop: 10},
  {bottom: 60, right: 20, marginTop: 10},
  {bottom: 5, right: 115, },
  {bottom: 62, left: 20, marginTop: 10},
  {left: 60, bottom: 20, marginTop: 10},
  {left: 5, top: 125, marginTop: -10},
  {top: 73, left: 18, marginTop: -10},
  {left: 57, top: 33, marginTop: -10},
  {top: 5, left: 125, marginLeft: -10}
];

export default class Watch extends Component {
  state = {
    hour: 'am',
    numbers: 'hours',
    hours: 2,
    minutes: 0
  }

  chooseNumber = (num) => {
    if(this.state.numbers === 'hours') {
      this.setState({hours: num});
    } else {
      this.setState({minutes: num});
    }
  }

  render() {
    let amStyle = {};
    let pmStyle = {};
    let numbers = null;
    if(this.state.hour === 'am') {
      amStyle = {opacity: 1};
      pmStyle = {opacity: 0.5};
    } else {
      amStyle = {opacity: 0.5};
      pmStyle = {opacity: 1};
    }
    if(this.state.numbers === 'hours') {
      numbers = hours;
    } else {
      numbers = minutes;
    }
    return (
      <View style={{flexDirection: 'row', backgroundColor: '#61B6D2', flex: 1}}>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View
            style={{
              backgroundColor: '#c5e4ec',
              height: 280,
              width: 280,
              borderRadius: 280,
              marginBottom: 15,
              marginTop: 15,
              position: 'relative',
              marginLeft: 10,
            }}
            >
            <View style={{
              position: 'absolute',
              top: 120,
              left: 100,
              width: 80,
              flexDirection: 'row',
              justifyContent: 'center'
              }}>
              <Text style={[{
                fontSize: 35
                }, this.state.numbers === 'hours' ? {color: '#47A3B8'} : {color: '#276a80'}
                ]}>{hours[this.state.hours]}</Text>
              <Text style={{
                fontSize: 35,
                color: '#276a80'
                }}>:</Text>
              <Text style={[{
                fontSize: 35
                }, this.state.numbers === 'minutes' ? {color: '#47A3B8'} : {color: '#276a80'}
                ]}>{minutes[this.state.minutes]}</Text>
            </View>
            {numbers.map((num, index) => {
              let selected = false;
              if(this.state.numbers === 'hours') {
                if(this.state.hours === index) {
                  selected = true;
                }
              } else {
                if(this.state.minutes === index) {
                  selected = true;
                }
              }
              return <WatchNumber
                press={this.chooseNumber}
                number={num}
                key={num}
                selected={selected}
                style={numsStyle[index]}/>;
            })}
          </View>
        </View>
        <View style={{justifyContent: 'center', paddingRight: 15}}>
          <TouchableHighlight
            onPress={() => this.setState({hour: 'am'})}
            >
            <Text style={[{
              color: '#fff',
              fontWeight: 'bold',
              fontSize: 18,
              textAlign: 'center',
              marginBottom: 5
            }, amStyle]}>AM</Text>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.setState({hour: 'pm'})}
            >
            <Text style={[{
              color: '#fff',
              fontWeight: 'bold',
              fontSize: 18,
              textAlign: 'center'
            }, pmStyle]}>PM</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}