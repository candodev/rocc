import React, { Component } from 'react';
import { Text, ScrollView, TouchableHighlight, AlertIOS, View as NView, Modal } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, InputGroup, Input, Button, View, Header } from 'native-base';
import TextField from 'react-native-md-textinput';
import Calendar from 'react-native-calendar';
import Icon from 'react-native-vector-icons/FontAwesome';
import { setUser } from '../../actions/user';
import { openDrawer } from '../../actions/drawer';
import styles from './style-reminder';
import Icons from 'react-native-vector-icons/MaterialIcons';
import Filters from '../filters';
import CustomTitle from './customtitle';
import Events from '../events';
import navigateTo from '../../actions/sideBarNav';
import Watch from './watch';

const calendarDays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
const calendarMonths = ['Jan', 'Feb', 'Mar', 'Apr' , 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const {
  replaceAt,
  popRoute
} = actions;

const customStyle = {
  calendarControls: {
    backgroundColor: '#61B6D2'
  },
  title: {
    color: 'white'
  },
  currentDayCircle: {
    backgroundColor: '#47A3B8'
  },
  day: {
    color: '#286A80'
  },
  dayHeading: {
    fontWeight: 'bold',
    color: '#47A3B8'
  },
  weekendHeading: {
    fontWeight: 'bold',
    color: '#47A3B8'
  },
  eventIndicator: {
    backgroundColor: '#47A3B8',
    width: 5,
    height: 5,
    borderRadius: 50
  },
  header: {
    backgroundColor: '#47A3B8',
    flexDirection: 'row'
  },
  icons: {
    color: '#fff'
  }
};

class Reminder extends Component {

  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      showAdvice: true,
      showCalendar: true,
      showFilters: false,
      currentFilter: 'all',
      calendar: false,
      modal: false
    };
  }

  setUser(name) {
    this.props.setUser(name);
  }

  replaceRoute(route) {
    this.props.replaceAt('calendar', { key: route }, this.props.navigation.key);
  }

  popRoute = () => {
    this.props.navigateTo('list', 'calendar');
  }

  hideAdvice = () => {
    this.setState({showAdvice: false});
  }

  toggleCalendar = () => {
    this.setState({showCalendar: !this.state.showCalendar});
  }

  toggleFilters = () => {
    this.setState({showFilters: !this.state.showFilters});
  }

  setFilter = (filter) => {
    this.setState({currentFilter: filter});
  }

  showCalendar() {
    return (
      <Calendar
        showEventIndicators
        customStyle={customStyle}
        ref="calendar"
        eventDates={['2017-01-03', '2017-01-06', '2017-01-10', '2017-01-15', '2017-01-21', '2017-01-25', '2017-01-28']}
        events={[{date: '2016-07-04', hasEventCircle: {backgroundColor: 'powderblue'}}]}
        scrollEnabled={true}
        showControls={true}
        dayHeadings={calendarDays}
        monthNames={calendarMonths}
        titleFormat={'YYYY'}
        onDateSelect={(date) => this.setState({ selectedDate: date })}
        weekStart={0}
        style={{borderBottomColor: '#ccc', borderBottomWidth: 2, flex: 1}}
        />
    );
  }

  render() {
    return (
      <Container>
        <View style={styles.container}>
          <Modal
            animationType={"fade"}
            transparent={true}
            visible={this.state.modal}
            onRequestClose={() => {alert("Modal has been closed.")}}
            >
            <NView style={{
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              position: 'absolute',
              zIndex: 9
            }}>
              <View style={{
                marginTop: 150,
                paddingLeft: 5,
                paddingRight: 5,
                backgroundColor: '#61B6D2'
                }}>
                <Watch />
              </View>
              <View style={{
                backgroundColor: '#61B6D2',
                justifyContent: 'flex-end',
                flexDirection: 'row',
                paddingBottom: 10,
                paddingRight: 20
                }}>
                <TouchableHighlight
                  onPress={() => this.setState({modal: false})}
                  >
                  <Text style={{fontWeight: 'bold', color: '#fff', fontSize: 20}}>OK</Text>
                </TouchableHighlight>
              </View>
            </NView>
          </Modal>
          <View style={{
            backgroundColor: '#47A3B8',
            marginTop: 20,
            paddingTop: 10,
            paddingBottom: 10
            }}>
            <TouchableHighlight
              onPress={() => {
                this.popRoute();
              }}
              style={{
                marginLeft: 20,
                width: 40
              }}
              >
              <Icon name="chevron-left" style={{color: '#fff'}}/>
            </TouchableHighlight>
          </View>
          <View style={[customStyle.calendarControls, {flexDirection: 'row', justifyContent: 'center'}]}>
            <TouchableHighlight
              onPress={() => this.setState({calendar: true })}
              style={this.state.calendar ? styles.monthAct : styles.month}>
              <Text
                style={this.state.calendar ? styles.txtMonthAct : styles.txtMonth}>
                Once
              </Text>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => this.setState({calendar: false })}
              style={!this.state.calendar ? styles.monthAct : styles.month}>
              <Text
                style={!this.state.calendar ? styles.txtMonthAct : styles.txtMonth}>
                Weekly
              </Text>
            </TouchableHighlight>
          </View>
          <View>
            <View style={styles.scrollStyle}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <View style={styles.scrollRow}>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Sun</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Mon</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Tue</Text>
                  </View>
                  <View style={styles.monthAct}>
                    <Text style={styles.txtMonthAct}>Wen</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Thu</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Fri</Text>
                  </View>
                  <View style={styles.month}>
                    <Text style={styles.txtMonth}>Sun</Text>
                  </View>
                </View>
              </ScrollView>
            </View>
            <View>
              {!this.state.calendar ? <Watch /> : this.showCalendar()}
            </View>
          </View>
          <View style={styles.cardsEnd}>
            {this.state.calendar ? (
              <TouchableHighlight
                onPress={() => this.setState({modal: !this.state.modal})}
                >
                <NView style={styles.cardHour}>
                  <View style={styles.oneCircle}>
                    <Icons name="watch" style={styles.iconCircle} />
                  </View>
                  <Text>
                    <Text style={styles.txtHour}>3 : 00  <Text style={styles.meredian}>PM</Text></Text>
                  </Text>
                </NView>
              </TouchableHighlight>
            ) : null}
            <View style={styles.cardRemind}>
              <View style={styles.firstPadd}>
                <Text style={styles.txtWhite}>Remind me to call <Text style={styles.txtBold}>Beto Sánchez @{'\n'}Home Today </Text><Text>at </Text><Text style={styles.txtBold}>3:00 pm</Text></Text>
              </View>
              <View style={styles.btnCenter}>
                <Button style={styles.btnGo} onPress={() => {
                  AlertIOS.alert(
                     'Saving reminder',
                     'Keep your app up to date to enjoy the latest features',
                     [
                       {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                       {text: 'Save', onPress: () => this.props.navigateTo('calendar', 'calendar')},
                     ],
                  );
                }}>
                  <Text style={styles.txtGo}>Ready!</Text>
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: name => dispatch(setUser(name)),
    openDrawer: () => dispatch(openDrawer()),
    popRoute: key => dispatch(popRoute(key)),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(Reminder);
