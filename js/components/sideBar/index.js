
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Content, Text, List, ListItem } from 'native-base';
import {Linking} from 'react-native';

import { setIndex } from '../../actions/list';
import navigateTo from '../../actions/sideBarNav';
import myTheme from '../../themes/base-theme';

import styles from './style';

class SideBar extends Component {

  static propTypes = {
    // setIndex: React.PropTypes.func,
    navigateTo: React.PropTypes.func,
  }

  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }

  render() {
    return (
      <Content theme={myTheme} style={[styles.sidebar, {backgroundColor: '#61B6D2'}]} >
        <List>
          <ListItem button onPress={() => this.navigateTo('home')} >
            <Text style={{color: '#fff'}}>Home</Text>
          </ListItem>
          <ListItem button onPress={() => {
            Linking.openURL('https://google.com.mx').catch(err => console.error('An error occurred', err));
            }} >
            <Text style={{color: '#fff'}}>Rate the app</Text>
          </ListItem>
          <ListItem>
            <Text style={{color: '#fff'}}>Logout</Text>
          </ListItem>
        </List>
      </Content>
    );
  }
}

function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(SideBar);
